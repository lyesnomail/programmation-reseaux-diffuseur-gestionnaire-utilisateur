import java.net.*;
import java.io.*;
import java.util.*;


class Message{ // Classe pour les messages
    private String id;
    private int nummess;
    private String mess;
    
    public Message(String id,int nummess,String mess){
        this.id = id;
        this.mess = mess;
        this.nummess =nummess;
    }
    String oldm(){
        return "OLDM "+Diffuseur.convgauche(""+nummess, 4, '0') +" "+Diffuseur.convdroite(id, 8, '#') +" "+ Diffuseur.convdroite(mess, 140, '#') ;
    }
    String diff(){
        return "DIFF "+Diffuseur.convgauche(""+nummess, 4, '0') +" "+Diffuseur.convdroite(id, 8, '#') +" "+ Diffuseur.convdroite(mess, 140, '#') ;
    }
    String mess(){
        return "MESS "+Diffuseur.convdroite(id, 8, '#') +" "+ Diffuseur.convdroite(mess, 140, '#') ;
    }
    int getnum(){return nummess;}
    String getid(){return id;} 
    String getmess(){return mess;} 
}

class Gest extends Thread { // classe pour objet gestionnaire
    private int port;
    private String ip;
    public int getPort() {
        return port;
    }
    public String getIp() {
        return ip;
    }
    
    public  void watcher(BufferedReader br,PrintWriter pw, Socket socket) throws IOException{//Méthode pour répondre au RUOK par IMOK
        String mes;
        while(true){
            mes = br.readLine();
            if(mes!=null){
                System.out.println(mes);
                pw.print("IMOK\r\n");
                pw.flush(); 
            }
            else {System.out.println("Connexion terminé!");socket.close();
                  break;}                    
        }
    }
    public  void regtogest(Diffuseur_objet o,Scanner scanner){//Méthode pour s'enregistrer dans un gestionnaire 
        try{        
            System.out.println("Veuillez insèrer un numèro de port de gestionnaire <9999");
            int port = scanner.nextInt();
            while (port>9999){
                System.out.println("Port incorrecte veuillez reessayer ! ( <=9999 )"); 
                port = scanner.nextInt(); 
            } 
            System.out.println("Veuillez insèrer une adresse de gestionnaire");scanner.nextLine();
            String ipp= scanner.nextLine();
            Socket socket=new Socket(ipp,port);
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));           
            pw.print(o.register()+"\r\n");
            pw.flush();
            String buff = br.readLine();
            if(buff!=null){
                
                if (buff.equals("REOK")){ 
                    System.out.println(buff);
                    Thread t = new Thread(()->{
                        try {
                            watcher(br,pw,socket);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                    t.start();
                }
                else{
                    System.out.println(buff);
                } }
        }
        catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
        }  
    }

}
class Diffuseur_objet { //Classe pour notre diffuseur 
    private String id;
    private int port;
    private String ipv4;
    private int portmulti;
    private String ipmult;
    public static List<Message> listmess;

    public Diffuseur_objet(String id,int port,String ipv4,int portmulti,String ipmulti) {
        this.id = id;
        this.port=port;
        this.ipv4=ipv4;
        this.portmulti=portmulti;
        this.ipmult = ipmulti;
    }
    public String getId() {
        return id;
    }
    public int getPort() {
        return port;
    }
    public String getIpv4() {
        return ipv4;
    }
    public int getPortMulti() {
        return portmulti;
    }
    public String getIpmult() {
        return ipmult;
    }
    public String register() {
        return "REGI"+" "+ Diffuseur.convdroite(id , 8, '#') +" "+ Diffuseur.convip(ipv4) +" "+ Diffuseur.convgauche(""+port, 4, '0') +" "+ Diffuseur.convip(ipmult) +" "+ Diffuseur.convgauche(""+portmulti, 4, '0')  ;
    }
    public void difftocli(Scanner scanner){ //Methode pour le multicast et l'ajout de message depuis le diffuseur
        try{    
            while(true){
                byte[]data;         
                DatagramSocket dso=new DatagramSocket();    
                System.out.println("Voules vous (numero): \n1- Inserer un nouveau message \n2- Envoyer un message\n3- Retour"); 
                int choix = scanner.nextInt(); 
                InetSocketAddress ia=new InetSocketAddress(getIpmult(),getPortMulti());       
                if(choix == 1) {
                    System.out.println("Veuillez inserer un message"); 
                    scanner.nextLine();
                    String mess = scanner.nextLine();
                    Message m = new Message( id, ((Diffuseur_objet.listmess.size()) % 9999)+1, mess);
                    Diffuseur_objet.listmess.add(m);
                }
                else if(choix == 2){
                    Message mm;
                    for(int j = Diffuseur_objet.listmess.size()-1;j>= (int)(Diffuseur_objet.listmess.size()-Diffuseur_objet.listmess.size() % 9999) ;j-- )
                    {   mm = Diffuseur_objet.listmess.get(j);
                        
                            System.out.println("/numero : " + mm.getnum() + " /id : " + mm.getid() + " /mess : " + mm.getmess() );
                       
                    }  
                    System.out.println("Veuillez inserer un numero de message ( il ya "+Diffuseur_objet.listmess.size() % 9999 + " messages ) ");             
                   
                    int choixmess = scanner.nextInt();
                    while (choixmess > Diffuseur_objet.listmess.size() % 9999 ) {
                        System.out.println("Veuillez inserer un numero de message < "+Diffuseur_objet.listmess.size() % 9999 );             
                        choixmess = scanner.nextInt();
                    }          
                    Message m = null; 
         
                    for(int j = Diffuseur_objet.listmess.size()-1;j>= (int)(Diffuseur_objet.listmess.size()-Diffuseur_objet.listmess.size() % 9999) ;j-- )
                    {   m = Diffuseur_objet.listmess.get(j);
                        if(m.getnum()==choixmess){
                            break;
                        }
                    }               
                    String mess = m.diff();
                    data=mess.getBytes();
                    DatagramPacket paquet=new 
                            DatagramPacket(data,data.length,ia);
                    dso.send(paquet);        
                    dso.close();          
                }else if(choix == 3){
                    break; 
                }           
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
 
    }
    public void connexion(Socket socket){ //Méthode de traitement des messages avec un utilisateur
        try
        {
            BufferedReader br=new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter pw=new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            while(true){                         
                String buff=br.readLine();
                if(buff!=null){
                    System.out.println(buff);
                    List<String> listword = new ArrayList<String>(Arrays.asList(buff.split(" ")));
                    
                        if (listword.get(0).equals("LAST")) {
                            Message lastm;
                            for(int j = Diffuseur_objet.listmess.size()-Integer.parseInt(listword.get(1));j<Diffuseur_objet.listmess.size() ;j++){
                                lastm = Diffuseur_objet.listmess.get(j);
                                pw.print(lastm.oldm()+"\r\n");
                                pw.flush(); 
                                Thread.sleep(2000);
                            }
                            pw.print("ENDM"+"\r\n");
                            pw.flush(); 
                        }
                        else if (listword.get(0).equals("MESS")){
                            pw.print("ACKM"+"\r\n");
                            pw.flush(); 
                            String messtosave = new String(buff);
                            String messtosave1 = messtosave.replaceAll("MESS "+listword.get(1)+" ", "");
                            messtosave1 = Diffuseur.deconv(messtosave1);
                            Message m = new Message(Diffuseur.deconv(listword.get(1)),Diffuseur_objet.listmess.size()+1,messtosave1);
                            Diffuseur_objet.listmess.add(m);
                        }
                        else { 
                            System.out.println("Erreur type message");
                        }                                 
                }
                else
                {break;}
            }
            System.out.println("Connexion terminé!");

        }
        catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
        }      
    }
    public  void lunchnewgest(Diffuseur_objet o,Scanner scanner) {   //Méthode choix entre s'inscrire à un gestionnaire et envoie de message au diffuseur
        try{  Diffuseur.iscon = false;   
        while(true){                  
                System.out.println("Voulez vous (numéro) : 1- S'inscrire a un nouveau gestionnaire 2- Envoyer un message à diffuser à tout les clients ");
                int input = scanner.nextInt();  
                if(input==1){
                    Diffuseur.iscon = true;                     
                    (new Gest()).regtogest(o,scanner);                               
                }      
                else if((input==2)&&(Diffuseur.iscon == true)){
                    this.difftocli(scanner);               
                }  
                else {
                    System.out.println("Erreur gestionnaire non insérée ou choix numéro");  
                }
            }        
        }
        catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
        } 
    }
    public void waitcnx(){ //Méthode multi thread utilisateurs
        try{
            ServerSocket server = new ServerSocket(this.getPort());
            while(true){
                Socket socket=server.accept();
                Thread t2 = new Thread(()->this.connexion(socket) );
                t2.start();            
            }
        }
        catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
        }  
    }
}

public class Diffuseur { //Classe contenant le main
    public static int lastid;
    public static boolean iscon;
    static String convgauche(String buf,int lim, char c){//Méthode d'ajout d'un caractère C a gauche
        String ippart;
        if (lim-buf.length()>0){
            String ippart2 = new String (new char[lim-buf.length()]).replace('\0',c);
            ippart =  ippart2 + buf ;
            }
            else { ippart = buf;}
        return ippart;
    }
    static String convdroite(String buf,int lim, char c){//Méthode d'ajout d'un caractère C a droite
        String ippart;
        if (lim-buf.length()>0){
            String ippart2 = new String (new char[lim-buf.length()]).replace('\0',c);
            ippart = buf + ippart2;
            }
            else { ippart = buf;}
        return ippart;
    }
    static String convip(String buf){//Méthode qui convertie une ip au format xxx.xxx.xxx.xxx
        String buf1 = new  String(buf);
        String buf2=buf1.replace('.', ' ');
        List<String> listword = new ArrayList<String>(Arrays.asList(buf2.split(" ")));
        String ipconv = convgauche(listword.get(0),3,'0') + "." + convgauche(listword.get(1),3,'0') + "." + convgauche(listword.get(2),3,'0') +"."  + convgauche(listword.get(3),3,'0');
        return ipconv;
    }
    static int postd(String buf){//Méthode qui retourne la (position + 1) du dernier caractère avant le #
        for(int i=buf.length()-1;i!=-1;i--){
            if (buf.charAt(i)!='#'){
                return i+1;
            }
        }
        return 0;
    }
    static String deconv(String buf){//Méthode qui enleve les # d'une chaine à partir d'une position p
        String buf2 = new String (buf);
        int p = postd(buf);
        if (p!=0){
            buf2 = buf2.substring(0, p);
            return buf2;
        }
        return buf;
    }
    static String deconvip(String buf){//Méthode qui rends au format original une adresse ip (exp : de 192.168.001.050 a 192.168.1.50)
        String buf1 = new  String(buf);
        String buf2=buf1.replace('.', ' ');
        List<String> listword = new ArrayList<String>(Arrays.asList(buf2.split(" ")));
        int ip1 = Integer.parseInt(listword.get(0));
        int ip2 = Integer.parseInt(listword.get(1));
        int ip3 = Integer.parseInt(listword.get(2));
        int ip4 = Integer.parseInt(listword.get(3));
        String ip = ip1 + "." + ip2 +"." + ip3 + "." + ip4;
        return ip;
    }
    public static void main(String args[]) throws IOException{
        lastid=0;
        System.out.println("Diffuseur : Bienvenu ! ようこそ ! "); 
        System.out.println("Creation d'un diffuseur"); 
        Scanner scanner = new Scanner( System.in );       

        System.out.println("Veuillez insérer un identifiant ( 8 caractères  )"); 
        String id = scanner.nextLine();   
        while (id.length()>8){
            System.out.println("Taille incorrecte veuillez reessayer ! ( 8 caractères  )"); 
            id = scanner.nextLine();  
        } 
        System.out.println("Veuillez insérer une adresse (Soit sur votre réseau (com : hostname -I) soit sur machine)"); 
        String ip = scanner.nextLine();
        System.out.println("Veuillez insérer une adresse de multidiff (exmp : 225.1.2.4)"); 
        String ipmult = scanner.nextLine();
        System.out.println("Veuillez insérer un numéro de port <=9999"); 
        int port = scanner.nextInt(); 
        while (port>9999){
            System.out.println("Port incorrecte veuillez reessayer ! ( <=9999 )"); 
            port = scanner.nextInt(); 
        } 
        System.out.println("Veuillez insèrer un numèro de port de multidiff <9999");
        int portmulti = scanner.nextInt();
        while (port>9999){
            System.out.println("Port incorrecte veuillez reessayer ! ( <=9999 )"); 
            portmulti = scanner.nextInt(); 
        } 
        Diffuseur_objet diff = new Diffuseur_objet( id, port,deconvip(ip), portmulti, deconvip(ipmult));
        Diffuseur_objet.listmess = new ArrayList<Message>();  
        Thread t2 = new Thread(()->diff.waitcnx());
        t2.start();
        diff.lunchnewgest(diff,scanner);
    }
}