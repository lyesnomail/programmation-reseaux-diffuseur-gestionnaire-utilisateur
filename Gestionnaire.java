import java.net.*;
import java.io.*;
import java.util.*;
class Diff{ //Classe objet pour un diffuseur
    private String id;
    private int port;
    private String ipv4 ;
    private int portmulti ;
    private String ipmult;
    public Diff(String id,String ip1,int port1, String ip2, int port2){
        this.id = id;
        this.ipv4 = ip1;
        this.port = port1 ;
        this.ipmult = ip2;
        this.portmulti = port2;
    }  
    public String getId() {
        return id;
    }
    public int getPort() {
        return port;
    }
    public String getIp() {
        return ipv4;
    }
    public int getPortMulti() {
        return portmulti;
    }
    public String item() {
        return "ITEM"+" "+Gestionnaire.convzerodroite(id, 8, '#') +" "+Gestionnaire.convip(ipv4)+" "+Gestionnaire.convzerogauche(""+port, 4, '0') +" "+ Gestionnaire.convip(ipmult)+" "+ Gestionnaire.convzerogauche(""+portmulti, 4, '0');
    }
    
}


class Gestionnaire_object extends Thread{ //Classe pour notre gestionnaire
    public static List<Diff> listdiff;
    private int port;
    private int nbr_max_cnx ;  
    Gestionnaire_object(int port, int max){
        this.port=port;
        nbr_max_cnx=max;
    } 
    public int getMax() {
        return nbr_max_cnx;
    }
    public int getPort() {
        return port;
    }
    public boolean register(Diff d,PrintWriter pw){//Méthode qui insert un diffuseur dans la liste des diffuseurs
        if(listdiff.size()<nbr_max_cnx){
            listdiff.add(d);  
            System.out.println("Diffuseur inseré");
            return true;
        }
        else{
            System.out.println("Liste pleine");
            pw.print("RENO\r\n");
            pw.flush();
            return false;
        }
    }

    public void retire(Diff d ){//Méthod qui retire un diffuseur d'une liste si il n'est plus connecté
        listdiff.remove(d);
        System.out.println("Diffuseur " + d.getId() +" absent et retiré de la liste !");

    }

    public boolean testtime(BufferedReader br) throws IOException{//Méthode qui test les IMOK
        String buff=br.readLine();
        if(buff!=null){
            if (buff.equals("IMOK")){
                System.out.println(buff);
                return true;
            }
            else{return false;}
        }
        else{
            return false;
        }
    }

    public void connexion(Socket socket){//Méthode pour connexon avec un diffuseur
        try
        {
            boolean connected = true;
            BufferedReader br=new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter pw=new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
          
           while(connected){   
            
                String buff=br.readLine();
                if(buff!=null){
                    System.out.println(buff);
                    List<String> listword = new ArrayList<String>(Arrays.asList(buff.split(" ")));
                    String firstelement = listword.get(0);                
                        if (firstelement.equals("REGI")){ 
                            
                            Diff diff = new Diff( Gestionnaire.deconv(listword.get(1)),Gestionnaire.deconvip(listword.get(2)), Integer.parseInt(listword.get(3)),Gestionnaire.deconv(listword.get(4)),Integer.parseInt(listword.get(5)));
                            boolean ind = register(diff,pw);
                            if (ind) {
                                pw.print("REOK\r\n");
                                pw.flush();
                                while(true){
                                    pw.print("RUOK\r\n");
                                    pw.flush();
                                    Date date = new Date(); 
                                    long mtn = date.getTime();
                                    boolean test = testtime(br);
                                    long mtn2 = date.getTime();
                                    if (((mtn2-mtn)>60000)||(!test)){
                                        retire(diff);
                                        connected=false;
                                        break;
                                    }      
                                
                                Thread.sleep(25000);
                                }                              
                            }
                        }
                        else if (firstelement.equals("LIST")){
                            pw.print("LINB "+Gestionnaire.convzerogauche("" + listdiff.size(), 2, '0')+"\r\n" );
                            pw.flush(); 
                            for(int j=0;j<listdiff.size();j++){
                                
                                pw.print((listdiff.get(j).item())+"\r\n");
                                pw.flush(); 
                                Thread.sleep(1000);
                            }}
                        else{
                            System.out.println("Erreur type message");
                        }
                        
                }
                else {break;}                 
            }
            br.close();
            pw.close();
            socket.close();
            System.out.println("Connexion terminé!");
        }
        catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
        }
    }
}


public class Gestionnaire{
    static int postd(String buf){//Méthode qui retourne la (position + 1) du dernier caractère avant le #
        for(int i=buf.length()-1;i!=-1;i--){
            if (buf.charAt(i)!='#'){
                return i+1;
            }
        }
        return 0;
    }
    static String deconv(String buf){//Méthode qui enleve les # d'une chaine à partir d'une position p
        String buf2 = new String (buf);
        int p = postd(buf);
        if (p!=0){
            buf2 = buf2.substring(0, p);
            return buf2;
        }
        return buf;
    }
    static String deconvip(String buf){//Méthode qui rends au format original une adresse ip (exp : de 192.168.001.050 a 192.168.1.50)
        String buf1 = new  String(buf);
        String buf2=buf1.replace('.', ' ');
        List<String> listword = new ArrayList<String>(Arrays.asList(buf2.split(" ")));
        int ip1 = Integer.parseInt(listword.get(0));
        int ip2 = Integer.parseInt(listword.get(1));
        int ip3 = Integer.parseInt(listword.get(2));
        int ip4 = Integer.parseInt(listword.get(3));
        String ip = ip1 + "." + ip2 +"." + ip3 + "." + ip4;
        return ip;
    }
    static String convzerogauche(String buf,int lim, char c){//Méthode d'ajout d'un caractère C a gauche
        String ippart;
        if (lim-buf.length()>0){
            String ippart2 = new String (new char[lim-buf.length()]).replace('\0',c);
            ippart =  ippart2 + buf ;
            }
            else { ippart = buf;}
        return ippart;
    }
    static String convzerodroite(String buf,int lim, char c){//Méthode d'ajout d'un caractère C a droite
        String ippart;
        if (lim-buf.length()>0){
            String ippart2 = new String (new char[lim-buf.length()]).replace('\0',c);
            ippart = buf + ippart2;
            }
            else { ippart = buf;}
        return ippart;
    }
    static String convip(String buf){//Méthode qui convertie une ip au format xxx.xxx.xxx.xxx
        String buf1 = new  String(buf);
        String buf2=buf1.replace('.', ' ');
        List<String> listword = new ArrayList<String>(Arrays.asList(buf2.split(" ")));
        String ipconv = convzerogauche(listword.get(0),3,'0') + "." + convzerogauche(listword.get(1),3,'0') + "." + convzerogauche(listword.get(2),3,'0') +"."  + convzerogauche(listword.get(3),3,'0');
        return ipconv;
    }
    public static void main(String args[]){
        System.out.println("Gestionnaire : Bienvenu ! bieherzlich willkommen ! Azul !"); 
        try (Scanner scanner = new Scanner( System.in ) ) {
            System.out.println("Veuillez insérer un numéro de port <=9999 "); 
            int port = scanner.nextInt();
            while (port>9999){
                System.out.println("Port incorrecte veuillez reessayer ! ( <=9999 )"); 
                port = scanner.nextInt(); 
            } 
            System.out.println("Veuillez inserer un nombre maximum de connexion (<=99)"); 
            int max = scanner.nextInt();
            while (max>99){
                System.out.println("Limite incorrecte veuillez reessayer ! ( <=99 )"); 
                max = scanner.nextInt(); 
            } 
            Gestionnaire_object.listdiff = new ArrayList<Diff>();              
            Gestionnaire_object gest = new Gestionnaire_object( port, max);
            try{
                ServerSocket server = new ServerSocket(gest.getPort());
                while(true){
                    Socket socket=server.accept();
                    Thread t = new Thread(()->gest.connexion(socket));
                    t.start();
                    
                }
            }
            catch(Exception e){
                System.out.println(e);
                e.printStackTrace();
            }
        }
    }
}