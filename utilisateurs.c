#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>

struct diff{ //Structure d'un diffuseur 
     char * id;
     int port;
     char * ipv4 ;
     int portmulti ;
     char * ipmult;
};
struct msg{//Structure d'un message 
    int numsg;
    char * msg;
};
int posd(char * buf,int lim ){ //Fonction qui retourne la position du dernier caractère avant #
    int i ;
    for(i=lim ;i!=-1; i--){
        if (buf[i] != '#' ){
            return i+1;
        }
    }return lim+1;
}
char * convip (char * buf){//Fonction qui convertit l'ip en format xxx.xxx.xxx xxx ( ajout des 0 à gauche)
    char * ip1 = malloc(4*sizeof(char));
    char * ip2 = malloc(4*sizeof(char));
    char * ip3 = malloc(4*sizeof(char));
    char * ip4 = malloc(4*sizeof(char));
    strncpy(ip1,buf,3);
    strncpy(ip2,buf+4,3);
    strncpy(ip3,buf+8,3);
    strncpy(ip4,buf+12,3);
    int ipp1 = atoi(ip1);
    int ipp2 = atoi(ip2);
    int ipp3 = atoi(ip3);
    int ipp4 = atoi(ip4);
    memset(ip1,'\0',strlen(ip1));
    memset(ip2,'\0',strlen(ip2));
    memset(ip3,'\0',strlen(ip3));
    memset(ip4,'\0',strlen(ip4));
    sprintf(ip1,"%d",ipp1);
    sprintf(ip2,"%d",ipp2);
    sprintf(ip3,"%d",ipp3);
    sprintf(ip4,"%d",ipp4);
    char * ipconv = malloc(16*sizeof(char));
    strcat(ipconv,ip1);
    strcat(ipconv,".");
    strcat(ipconv,ip2);
    strcat(ipconv,".");
    strcat(ipconv,ip3);
    strcat(ipconv,".");
    strcat(ipconv,ip4);
    return ipconv;
}
int nbrword(char * buf)//Fonction qui détermine le nombre de sous mots dans une chaine séparé par des espaces
{   
    int nbr=0;
    char * bufcopy = malloc(strlen(buf));
    memset(bufcopy,'\0',strlen(bufcopy));
    strncpy(bufcopy,buf,strlen(buf));
    char * token = strtok(bufcopy," ");
    while (token != NULL)
    { nbr++;
      token = strtok(NULL," ");}
    free(bufcopy);free(token);
      return nbr;
}

char ** words(char * buf)//Fonction qui retourne un tableau de mots à partir d'une chaine constitué d'autres sous mots séparés par des espaces
{   
    char * bufcopy = malloc(strlen(buf)*sizeof(char));
    char ** t = malloc((nbrword(buf))*sizeof(char));
    memset(bufcopy,'\0',strlen(bufcopy)*sizeof(char));
    strncpy(bufcopy,buf,strlen(buf)*sizeof(char));
    char * token = strtok(bufcopy," ");
    int i = 0;
    while (token != NULL)
    {  
       t[i]=malloc(sizeof(token));   
       strncpy(t[i],token,strlen(token)*sizeof(char));
       token = strtok(NULL," ");i = i+1;}
    t[nbrword(buf)]="\0";
    free(bufcopy);free(token);
    return t;
}

struct diff  * find(struct diff t[] ,  char * d,int nbr){

    return NULL;
}
int exist (struct diff t[] ,  char * d,int nbr){ //
    int i;
    for(i=0;i<nbr;i++){
        if (strcmp(t[i].id,d)==0){
            return 1;
        }
    }
    return 0;
}
char * fill(int lim){ //Fonction qui retourne une chaîne de taille lim contenant des #
    char * buff = malloc(lim*sizeof(char));
    int i;
    for(i=0;i<lim;i++){
        buff[i]='#';
    }
    return buff;
}
char * fill2(int lim){//Fonction qui retourne une chaîne de taille lim contenant des 0
    char * buff = malloc(lim*sizeof(char));
    int i;
    for(i=0;i<lim;i++){
        buff[i]='0';
    }
    return buff;
}
char * tosend(char * msg,char * id){ // Fonction qui retourne une chaine type MESS ID MESSAGE
    char * m=malloc(200*sizeof(char));
    memset(m,'\0',strlen(m));
    strcat(m, "MESS ");
    if((8-strlen(id))!=0){
        
        int s1 = (8) * sizeof(char) - strlen(id)*sizeof(char);
        char * complet1 = fill(s1);
        strcat(m, id); 
        strcat(m, complet1);
    }
    else { strcat(m, id); }
    strcat(m, " ");
    strcat(m, msg);
    if((140-strlen(msg))!=0){
        
        int s2 = (140) * sizeof(char) - strlen(msg)*sizeof(char);
        char *  complet2 = fill(s2);
        strcat(m, complet2);
    }
    else { strcat(m, msg); }
    
    strcat(m, "\r");
    strcat(m, "\n");
    char * msgtosend=malloc(156*sizeof(char));
    int s5 = 157*sizeof(char);
    memset(msgtosend,'\0',s5);
    strncpy(msgtosend,m,156);
    return msgtosend;
}


void * messagemult(void * c){ // Fonction ( en thread) pour recevoir les messages diff
  struct diff * choix = (struct diff *)c;
  int sock=socket(PF_INET,SOCK_DGRAM,0);
  int ok=1;
  int r=setsockopt(sock,SOL_SOCKET,SO_REUSEPORT,&ok,sizeof(ok));
  if (r==-1){perror("ERREUR : messagemult");}
  struct sockaddr_in address_sock;
  address_sock.sin_family=AF_INET;
  address_sock.sin_port=htons((* choix).portmulti);
  address_sock.sin_addr.s_addr=htonl(INADDR_ANY);
  r=bind(sock,(struct sockaddr *)&address_sock,sizeof(struct sockaddr_in));
  struct ip_mreq mreq;
  mreq.imr_multiaddr.s_addr=inet_addr((* choix).ipmult);
  mreq.imr_interface.s_addr=htonl(INADDR_ANY);
  r=setsockopt(sock,IPPROTO_IP,IP_ADD_MEMBERSHIP,&mreq,sizeof(mreq));
  char tampon[300];
  while(1){   
    int rec=recv(sock,tampon,300,0);
    tampon[rec]='\0';
    tampon[strlen(tampon)]='\0';
    printf("%s\n",tampon);
  }
}
int main(){    
    char * id = malloc(100*sizeof(char));char buff2 [200];
    printf("Utilisateur : Bienvenu ! bienvenidos ! Welcome !\n");
    tryid : printf("Veuillez insérer un identifiant ( 8 caractères maximum ) \n");
    scanf("%s",id); 
    if (strlen(id)>8){ printf("Erreur longeur id > 8\n"); goto tryid; }
    choix2 : printf("Veuillez insérer le port d'un gestionnaire (<=9999)\n");
    int portgest;
    portret : scanf("%d",&portgest);
    if (portgest > 9999){printf("Veuillez inserer un port correct <=9999\n");goto portret; }
    printf("Veuillez insérer l'adresse d'un gestionnaire\n");
    char * ipgest = malloc(100*sizeof(char));
    scanf("%s",ipgest);
    struct sockaddr_in adress_sockgest;
    choix3:adress_sockgest.sin_family = AF_INET;
    adress_sockgest.sin_port = htons(portgest);
    inet_aton(ipgest,&adress_sockgest.sin_addr);  
    int descr=socket(PF_INET,SOCK_STREAM,0);
    int r=connect(descr,(struct sockaddr *)&adress_sockgest,
                    sizeof(struct sockaddr_in));
    if(r!=-1){     
        char buff[160];
        int sendlist;
        sendlist=send(descr,"LIST\r\n",strlen("LIST\r\n")*sizeof(char),0);
        if(sendlist==-1){perror("ERREUR : ENOVOIE LIST");}
        int linb=recv(descr,buff,160*sizeof(char),0); 
        
        if(linb==-1){perror("ERREUR : LIST NUMB");}
        char **  msg = words(buff);
        printf("LINB %s \n",msg[1]);  
        int nbr = atoi(msg[1]);
        struct diff t[nbr];
        int i;
        for(i=0;i<nbr;i++){
            memset(buff,'\0',sizeof(buff)); 
            
            int msglen=recv(descr,buff,160*sizeof(char),0);
            strcat(buff," 1");
            if(msglen==-1){perror("ERREUR : recv diff");}
            buff[strlen(buff)]='\0';
            t[i].id=words(buff)[1];
            t[i].ipmult=convip(words(buff)[4]);
            t[i].ipv4=convip(words(buff)[2]);
            t[i].port=atoi(words(buff)[3]);
            t[i].portmulti=atoi(words(buff)[5]);
            buff[strlen(buff)-1]='\0'; 
            printf("%s\n",buff);            
            sleep(1);
        }
        printf("Selectionner un diffuseur (identifiant) \n");
        char * choixdiff=malloc(sizeof(char)*200);
        tryagain :scanf("%s",choixdiff);
        for(i=0;i<nbr;i++){
            if (strncmp(t[i].id,choixdiff,posd(t[i].id,7))==0){
               break;
            }
        }
        if (i==nbr){
            printf("Ellement non existant ! veuillez réessayer \n");
            goto tryagain;
        }
        struct diff choix = t[i];
        printf("Fin de connexion avec le gestionnaire \n");     
        printf("Debut de connexion avec sdsds client \n");   
        close(descr);
        int p=choix.port;
        struct sockaddr_in adress_sock;
        adress_sock.sin_family = AF_INET;
        adress_sock.sin_port = htons(p);
        inet_aton(choix.ipv4,&adress_sock.sin_addr);  
        descr=socket(PF_INET,SOCK_STREAM,0);
        r=connect(descr,(struct sockaddr *)&adress_sock,
                        sizeof(struct sockaddr_in));
        if(r!=-1){
            pthread_t th;
            pthread_create(&th,NULL,messagemult,(void *)&choix);
            choix1 : tryagainchoices : printf("Voulez vous : (Tapez numero)\n1 - Envoyer un message \n2 - Recevoir la liste des messages\n");
            int choixaction; 
            scanf("%d",&choixaction);
            if(choixaction==1){
                memset(buff,'\0',sizeof(buff));
                tryagainmsg : printf("Tapez un message (< 140 caracteres) \n");
                char msgtosend [140];
                fgets(msgtosend,140, stdin);
                scanf ("%[^\n]%*c", msgtosend);                
                if(strlen(msgtosend)>140){printf("Erreur taille message > 140 caracteres veuillez réessayer \n"); goto tryagainmsg;}    
                char * message ;
                message = tosend(msgtosend,id);
                send(descr,message,strlen(message),0);
                int ackm =recv(descr,buff,160*sizeof(char),0);
                buff[ackm]='\0';
                printf("%s\n",buff);
            }
            else if(choixaction==2){
                printf("Combiens d'élements voulez vous obtenir ( <=999 ) ?\n");
                
                char * comb = malloc(200*sizeof(char));
                char * nbrelm = malloc(3*sizeof(char));
                lastre : scanf("%s",nbrelm);
                
                if (atoi(nbrelm)>999){ printf("Veuillez reessayer car nombre elements < 999\n"); goto lastre;}
                
                strcat(comb,"LAST ");
                if((3-strlen(nbrelm))!=0){
                    
                    int s3= (3) * sizeof(char) - strlen(nbrelm)*sizeof(char);
                    char * complet = fill2(s3) ; 
                    strcat(comb,complet);
                }
                strcat(comb,nbrelm);
                strcat(comb,"\r\n");
                char * tosend = malloc(strlen(comb)*sizeof(char));
                strncpy(tosend,comb,strlen(comb));
                int testsend = send(descr,tosend,strlen(tosend)*sizeof(char),0);
                if (testsend == -1) {
                    printf("Probleme envoie\n");
                }
                int s4;
                for(i=1;i<=atoi(nbrelm)+1;i++){
                    s4 = sizeof(buff2);
                    memset(buff2,'\0',s4);
                    int linb=recv(descr,buff2,200*sizeof(char),0);
                    if(linb==-1){perror("ERREUR : msg depuis diff ");}
                    printf("%s\n",buff2);
                    sleep(1);               
                }  
            }
            else{
                printf("Erreur choix !, veuillez réessayer \n");
                goto tryagainchoices;
            }     
            printf("Voulez vous : \n1- Executer une nouvelle action avec le serveur\n2- Vous connecter a un nouveau gestionnaire\n3- Vous connecter a un nouveau serveur\n4- Quitter\n");
            int choix;
            scanf("%d",&choix);
            if(choix == 1){
                goto choix1;
            }
            else if(choix == 2){
                close(descr);
                pthread_cancel(th);
                goto choix2;
            }
            else if (choix == 3){
                close(descr);
                pthread_cancel(th);
                goto choix3;
            }
            else if (choix == 4){
                close(descr);
                pthread_cancel(th);
                return 0;
            }       
        }
    }
    free(id);
    return 0;
}
