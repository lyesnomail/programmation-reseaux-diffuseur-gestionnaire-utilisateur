- Commandes pour compiler :
javac Diffuseur.java
javac Gestionnaire.java
gcc -Wall -pthread utilisateurs.c -o utilisateurs

- Pour exécuter :
java Diffuseur
java Gestionnaire
./utilisateurs

Membres :
Lyes FEZOUI 21965519
Mohamed Yanis SADAOUI 21986699
GROUPE 33 

- Architecture :
-Les parties Diffuseure / Gestionnaire sont codées en java, leurs structures sont similaires, un main qui remplit les caractéristiques d'un objet gestionnaire/diffuseur, qui invoque une méthode qui se met en écoute sur le réseau et réagi en fonction des messages qu'il recois ( notons que l'un  (gestionnaire) enregistre une liste des diffuseurs, et l'autre une liste des messages, et que le diffuseur se connect en multicast avec ses clients si besoin) .
-La partie utilisateurs est codée en C, le main peut se découper en  deux parties : une pour les interactions avec un gestionnaire, l'autre avec un diffuseur choisi, de plus notons qu'il est possible de se connecter à un autre diffuseur sans avoir à réexecuter ( notons aussi l'existence d'une fonction pour la réception en multicast des messages DIFF en thread) 

